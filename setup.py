try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def read_me():
    with open('README.md') as f:
        return f.read()

dependencies = []
tests_dependencies = []

package_info = {
    'name': '',
    'author': 'Shahab Rezaee',
    'author_email': 'shahabrezaee@gmail.com',
    'description': '',
    'long_description': read_me(),
    'license': 'AGPL v3',
    'classifiers': [
        'License :: OSI Approved :: AGPL v3',
        'Programming Language :: Python :: 3.5',
    ],
    'keywords': [],
    'url': '',
    'download_url': '',
    'version': '0.1.0',
    'install_requires': dependencies + tests_dependencies,
    'packages': ['NAME'],
    'scripts': [],
    'include_package_data': True,
    'zip_safe': False
}

setup(**package_info)

